import asyncio
from collections import defaultdict, deque
from enum import Enum
import itertools
import json
import logging

import aiohttp
import numpy
import websockets

api_logger = logging.getLogger('api')
fh = logging.FileHandler('api.log')
api_logger.addHandler(fh)
api_logger.setLevel(logging.INFO)

ticker_logger = logging.getLogger('ticker')
fh = logging.FileHandler('ticker.log')
ticker_logger.addHandler(fh)
ticker_logger.setLevel(logging.DEBUG)

fills_logger = logging.getLogger('fills')
fh = logging.FileHandler('fills.log')
fills_logger.addHandler(fh)
fills_logger.setLevel(logging.DEBUG)

status_logger = logging.getLogger('status')
fh = logging.FileHandler('status.log')
status_logger.addHandler(fh)
status_logger.setLevel(logging.DEBUG)


BASE_URL = 'https://api.stockfighter.io/ob/api'
GM_URL = 'https://api.stockfighter.io/gm'
WS_BASE_URL = 'wss://api.stockfighter.io/ob/api/ws'


class Urls(Enum):
    status = '/heartbeat'
    buy = '/venues/{venue}/stocks/{stock}/orders'
    sell = '/venues/{venue}/stocks/{stock}/orders'
    order_book = '/venues/{venue}/stocks/{stock}'
    quote = '/venues/{venue}/stocks/{stock}/quote'
    orders_status = '/venues/{venue}/accounts/{account}/orders'
    cancel = '/venues/{venue}/stocks/{stock}/orders/{order}'
    start_level = '/levels/{level}'
    gm_stop_level = '/instances/{instance}/stop'
    gm_resume_level = '/instances/{instance}/resume'
    gm_instance_status = '/instances/{instance}'
    ws_fills = '/{account}/venues/{venue}/executions'
    ws_ticker = '/{account}/venues/{venue}/tickertape'


class Position:
    shares = 0
    cash = 0
    nav = 0
    avg = 0
    last_buys = deque(maxlen=3)
    last_sells = deque(maxlen=3)

    @property
    def avg_operations(self):
        return numpy.mean(list(itertools.chain(
            self.last_buys, self.last_sells)))

    @property
    def avg_buys(self):
        return numpy.mean(self.last_buys)

    @property
    def avg_sells(self):
        return numpy.mean(self.last_sells)

    def log(self, direction, order_id, f_price):
        self.nav = f_price * self.shares + self.cash
        fills_logger.info(
            '{direction} @ {f_price} ID: {order_id} {shares} shares '
            '${cash:.2f} {nav:.2f} NAV avg {avg:.2f}'.format(
                direction=direction, order_id=order_id, shares=self.shares,
                cash=self.cash / 100, nav=self.nav / 100,
                avg=self.avg_operations / 100, f_price=f_price))


class Ticker:
    bid = None
    bid_depth = None
    bid_size = None
    ask = None
    ask_depth = None
    ask_size = None
    last = None
    last_size = None
    # last_trade = None
    # quote_time = None
    symbol = None
    venue = None

    def from_dict(self, dict_):
        self.bid = dict_['bid']
        self.bid_depth = dict_['bidDepth']
        self.bid_size = dict_['bidSize']
        self.ask = dict_['ask']
        self.ask_depth = dict_['askDepth']
        self.ask_size = dict_['askSize']
        self.last = dict_['last']
        self.last_size = dict_['lastSize']
        # self.last_trade = dict_['lastTrade']
        # self.quote_time = dict_['quoteTime']
        self.symbol = dict_['symbol']
        self.venue = dict_['venue']

    def log(self):
        str = 'Ticker: '
        str += ('BidD: {bid_depth:6} BidS: {bid_size:6} Bid -> {bid:6} {ask:6} <- Ask '
                'AskS: {ask_size:6} AskD: {ask_depth:6} Last: {last:6}'
                ' LastS: {last_size:6}').format(**self.__dict__)

        ticker_logger.debug(str)


class Api:
    def __init__(self, api_key=None, account=None, instance=None, level=None,
                 ticker=None, position=None):
        self.account = account
        self.api_key = api_key
        self.ticker = ticker
        self.position = position
        self.instance = instance
        self.level = level

    async def request(self, method, url, base_url=BASE_URL, **kwargs):
        full_url = base_url + url
        api_logger.debug(
            '%(method)s request to %(url)s',
            dict(method=method, url=full_url))
        json_params = kwargs.get('json')
        data = kwargs.get('data')
        if json_params:
            data = json.dumps(json_params)
            del kwargs['json']
        headers = kwargs.pop('headers', {})
        if self.api_key:
            headers.update({'X-Starfighter-Authorization': self.api_key})
        response = await aiohttp.request(
            method, full_url, data=data, headers=headers, **kwargs)
        try:
            if 'json' in response.headers['content-type']:
                return await response.json()
            else:
                return await response.read()
        except Exception as e:
            api_logger.exception(e)

    def post(self, url, **kwargs):
        return self.request('post', url, **kwargs)

    def delete(self, url, **kwargs):
        return self.request('delete', url, **kwargs)

    def get(self, url, **kwargs):
        return self.request('get', url, **kwargs)

    async def get_status(self):
        status_logger.info('Getting backoffice status')
        url = Urls.gm_instance_status.value.format(instance=self.instance)
        header = {'Cookie': 'api_key=%s' % self.api_key}
        response = await self.get(url, base_url=GM_URL, headers=header)
        backoffice = response.get(
            'flash', {}).get('info', 'NO INFO AVAILABLE')
        if '$' in backoffice:
            status_logger.info(backoffice)
        else:
            status_logger.info(response)
        return response

    async def get_orders_status(self, venue):
        api_logger.info('Getting all orders')
        url = Urls.orders_status.value.format(
            venue=venue, account=self.account)
        response = await self.get(url)
        for order in response.get('orders'):
            self.process_fills(order, self.position)
        api_logger.debug(response)
        return response

    async def stop_level(self):
        api_logger.info('Stopping level')
        url = Urls.gm_stop_level.value.format(instance=self.instance)
        header = {'Cookie': 'api_key=%s' % self.api_key}
        response = await self.post(url, base_url=GM_URL, headers=header)
        api_logger.debug(response)
        return response

    async def start_level(self, retries=0, wait=15):
        api_logger.info('Starting level')
        url = Urls.start_level.value.format(level=self.level)
        header = {'Cookie': 'api_key=%s' % self.api_key}
        response = await self.post(url, base_url=GM_URL, headers=header)
        max_retries = 3
        if (not response.get('ok') and retries < max_retries):
            retries += 1
            api_logger.info(
                'Level start failed. Retrying in %(wait)s seconds...',
                dict(wait=wait))
            await asyncio.sleep(wait)
            response = await self.start_level(retries)

        print(response)
        return response

    async def resume_level(self):
        api_logger.info('Resuming level')
        url = Urls.gm_resume_level.value.format(instance=self.instance)
        header = {'Cookie': 'api_key=%s' % self.api_key}
        response = await self.post(url, base_url=GM_URL, headers=header)
        return response

    async def check_api_status(self):
        api_logger.info('Checking API status')
        response = await self.get('/heartbeat')
        api_logger.debug(
            'API status: %(status)s', dict(status=response))
        return response.get('ok', False)

    async def get_quote(self, venue, stock):
        url = Urls.quote.value.format(venue=venue, stock=stock)
        response = await self.get(url)
        return response

    async def get_orderbook(self, venue, stock):
        url = Urls.order_book.value.format(venue=venue, stock=stock)
        response = await self.get(url)
        return response

    async def sell(self, venue, stock, price, qty, type):
        params = {
            'venue': venue,
            'stock': stock,
            'price': price,
            'qty': qty,
            'orderType': type,
            'account': self.account,
            'direction': 'sell',
        }
        url = Urls.sell.value.format(venue=venue, stock=stock)
        response = await self.post(url, json=params)
        if 'error' in response:
            api_logger.warning(response)
        else:
            api_logger.info(
                '%(id)6s %(venue)s - %(direction)4s: %(qty)s x %(symbol)s'
                ' @ %(price)s',
                response)
        return response

    async def buy(self, venue, stock, price, qty, type):
        params = {
            'venue': venue,
            'stock': stock,
            'price': price,
            'qty': qty,
            'orderType': type,
            'account': self.account,
            'direction': 'buy',
        }
        url = Urls.buy.value.format(venue=venue, stock=stock)
        response = await self.post(url, json=params)
        if 'error' in response:
            api_logger.warning(response)
        else:
            api_logger.info(
                '%(id)6s %(venue)s - %(direction)4s: %(qty)s x %(symbol)s'
                ' @ %(price)s',
                response)
        return response

    async def cancel(self, venue, stock, order):
        url = Urls.cancel.value.format(venue=venue, stock=stock, order=order)
        response = await self.delete(url)
        api_logger.info(
            'Cancel %(id)s - %(direction)4s: %(symbol)s @ %(price)s',
            response)
        return response

    async def connect_websocket(
            self, url, callback, retries=0, args=None, kwargs=None):
        if not args:
            args = []
        if not kwargs:
            kwargs = dict()
        api_logger.debug('Connecting websocket to %s', url)
        ws = await websockets.connect(url)
        api_logger.debug('Websocket ready')
        max_retries = 3
        while True:
            await asyncio.sleep(0.000001)
            data = await ws.recv()
            if not data:
                if retries <= max_retries:
                    api_logger.debug('Socket disconnected. Retrying...')
                    retries += 1
                    await self.connect_websocket(
                        url, callback, retries, args, kwargs)
                continue
            retries = 0
            json_data = json.loads(data)
            callback(json_data, *args, **kwargs)

    def process_fills(self, data, position):
        api_logger.info('Processing fills')
        if data.get('account') == self.account:
            order = data.get('order', data)
            direction = order.get('direction')
            fill = order.get('fills')
            for f in fill:
                f_price = f.get('price')
                qty = f.get('qty')
                if direction and direction == 'buy':
                    position.shares += qty
                    position.last_buys.append(f_price)
                    position.cash -= qty * f_price
                elif direction and direction == 'sell':
                    position.shares -= qty
                    position.last_sells.append(f_price)
                    position.cash += qty * f_price
                api_logger.info('Avg: %s', position.avg)
                position.log(direction, order.get('id'), f_price)

    async def get_fills(self, venue, stock, position):
        url = Urls.ws_fills.value.format(
            venue=venue, account=self.account, stock=stock)
        url = WS_BASE_URL + url
        await self.connect_websocket(url, self.process_fills, args=[position])

    def process_ticker(self, data, ticker):
        quote = data.get('quote', {})
        if quote:
            d_quote = defaultdict(lambda: 0, quote)
            quote['ask'] = quote.get('ask', 0)
            quote['bid'] = quote.get('bid', 0)
            ticker.from_dict(d_quote)
            ticker.log()

    async def get_ticker(self, venue, stock, ticker):
        url = Urls.ws_ticker.value.format(venue=venue, account=self.account)
        url = WS_BASE_URL + url
        await self.connect_websocket(url, self.process_ticker, args=[ticker])
