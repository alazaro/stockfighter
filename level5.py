import asyncio
import argparse
from collections import defaultdict, deque
import itertools

import logging

from api import Api, Ticker, Position


logger = logging.getLogger('asyncio')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)


QUEUE_LENGTH = 4


bought = defaultdict(deque)
sold = defaultdict(deque)


def adjust_prices(ticker, position):
    size = 60
    s_bid = size
    s_ask = size
    cancel_ask = False
    cancel_bid = False
    if ticker.bid_size == size:
        s_ask = 0
        ask = ticker.ask
    else:
        ask = ticker.ask - 5

    if ticker.ask_size == size:
        s_bid = 0
        bid = ticker.bid
    else:
        bid = ticker.bid + 5

    if (ask - bid) < 40:
        s_bid = 0
        s_ask = 0
        cancel_bid = True
        cancel_ask = True

    if ask == bid:
        bid -= 40

    if position.avg_operations and bid > position.avg_operations:
        s_bid = 0

    if position.avg_operations and ask < position.avg_operations:
        s_ask = 0

    if position.avg_operations - ticker.ask > 1000 and ticker.ask > 0:
        bid = ticker.ask
        s_bid = 200

    if position.avg_operations - ticker.bid < -1000 and ticker.bid > 0:
        ask = ticker.bid
        s_ask = 200

    if bid <= 0:
        bid = 1
        s_bid = size
    if ask <= 0:
        s_ask = 0
        ask = 1000000

    if position.shares > 200:
        s_bid = 0
        cancel_bid = True
    if position.shares < -200:
        s_ask = 0
        cancel_ask = True
    return ask, bid, s_ask, s_bid, cancel_ask, cancel_bid


async def cancel_orders(ask, bid):
    bought_keys_to_cancel = [k for k in bought.keys() if k > (bid + 5)]
    sold_keys_to_cancel = [k for k in sold.keys() if k < (ask - 5)]

    orders_to_cancel = itertools.chain(
        itertools.chain.from_iterable(
            bought.pop(k) for k in bought_keys_to_cancel),
        itertools.chain.from_iterable(
            sold.pop(k) for k in sold_keys_to_cancel))

    # Cancel buy orders before selling at the same price
    tasks = [
        loop.create_task(api.cancel(venue, stock, order.get('id')))
        for order in orders_to_cancel]
    asyncio.as_completed(asyncio.wait(tasks))


async def buy_loop(api, position, ticker, venue, stock):
    while True:
        while ticker.bid is None and ticker.ask is None:
            await asyncio.sleep(0.0001)

        ask, bid, s_ask, s_bid, cancel_ask, cancel_bid = adjust_prices(
            ticker, position)

        if cancel_ask:
            ask = 1000000
        if cancel_bid:
            bid = 0

        if s_ask > 0:
            response = await api.sell(
                venue, stock, ask, s_ask, 'limit')
            price = response.get('price')
            if price:
                sold[price].append(response)
        if s_bid > 0:
            response = await api.buy(
                venue, stock, bid, s_bid, 'limit')
            price = response.get('price')
            if price:
                bought[price].append(response)

        asyncio.ensure_future(cancel_orders(ask, bid))

        if not (s_ask > 0 or s_bid > 0):
            await asyncio.sleep(0.0001)

async def bookloop(api, venue, stock):
    while True:
        await asyncio.sleep(1)
        await api.get_orderbook(venue, stock)

async def status_loop(api):
    while True:
        await asyncio.sleep(5)
        await api.get_status()

async def main(options, ticker, position):
    level = 'irrational_exuberance'
    api_key = '7eebfee0e3e3cf8e5559652890e0ac120f7cc6fc'
    instance = 19762
    api = Api(api_key=api_key, level=level, ticker=ticker, position=position)
    api.instance = instance
    if options.start_new or options.resume:
        if options.start_new:
            result = await api.stop_level()
            result = await api.start_level()
        else:
            result = await api.resume_level()
            if not isinstance(result, dict) or not result.get('ok'):
                result = await api.start_level()
        stock = result.get('tickers', [])[0]
        venue = result.get('venues', [])[0]
        account = result.get('account')
        api.account = account
        orders = await api.get_orders_status(venue)
        if 'order' in orders:
            for order in orders.get('orders'):
                loop.create_task(api.cancel(venue, stock, order.get('id')))

    options = {
        'stock': stock,
        'venue': venue
    }
    return api, options


async def reset_avg(position):
    while True:
        await asyncio.sleep(120)
        logger.info('Resetting average')
        position.last_buys.clear()
        position.last_sells.clear()


if __name__ == '__main__':
    args = argparse.ArgumentParser()
    args.add_argument(
        '--start_new', action='store_true', default=False,
        help='Starts a new level')
    args.add_argument(
        '--resume', action='store_true', default=False,
        help='Resumes the level')
    parsed = args.parse_args()
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    ticker = Ticker()
    position = Position()
    main_func = asyncio.ensure_future(main(parsed, ticker, position))
    loop.run_until_complete(
        asyncio.gather(main_func, loop=loop))

    api, options = main_func.result()
    venue = options['venue']
    stock = options['stock']

    ticker_func = api.get_ticker(venue, stock, ticker)
    fills_func = api.get_fills(venue, stock, position)
    api.position = position
    api.ticker = ticker
    buy_func = buy_loop(api, position, ticker, venue, stock)
    status_func = status_loop(api)
    reset_avg_func = reset_avg(position)
    tasks = [
        asyncio.ensure_future(ticker_func),
        asyncio.ensure_future(status_func),
        asyncio.ensure_future(buy_func),
        asyncio.ensure_future(fills_func),
        asyncio.ensure_future(reset_avg_func),
    ]
    loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()
