import asyncio
import os
from pprint import pprint

from api import Api, Ticker, Position


async def __main__():
    level = 'dueling_bulldozers'
    api_key = '7eebfee0e3e3cf8e5559652890e0ac120f7cc6fc'
    instance = 8125
    ticker = Ticker()
    position = Position()
    api = Api(api_key=api_key, level=level, ticker=ticker, position=position)
    api.instance = instance
    result = await api.resume_level()
    stock = result.get('tickers', [])[0]
    venue = result.get('venues', [])[0]
    while True:
        result = await api.get_orderbook(venue, stock)
        pprint(result)
        asyncio.sleep(1)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(__main__()))
