import asyncio
import argparse
from collections import defaultdict, deque
import itertools

import logging

from api import Api


logger = logging.getLogger('asyncio')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)


BASE_URL = 'https://api.stockfighter.io/ob/api'
GM_URL = 'https://api.stockfighter.io/gm'

QUEUE_LENGTH = 10


bought = defaultdict(deque)
sold = defaultdict(deque)


def adjust_prices(api):
    s_bid = 1000
    s_ask = 50
    ask, bid = api.ask, api.bid
    if api.target:
        if ask < api.target:
            bid = ask + 1
            s_bid = 100000
        if bid < api.target:
            ask = api.target + 15

    if ask == bid:
        bid -= 10

    if api.target is not None and api.target < bid:
        bid = (api.target + bid) // 2
        ask = bid + 10
    elif api.bid is None != api.ask is None:
        api.ask = ask = 1
        api.bid = bid = 50
    if bid == 0:
        bid = 1
    return ask, bid, s_ask, s_bid


async def buy_loop(api, venue, stock, limit_bid, limit_ask):
    while True:
        while api.bid is None and api.ask is None:
            await asyncio.sleep(1)

        ask, bid, s_ask, s_bid = adjust_prices(api)

        bought_keys_to_cancel = [k for k in bought.keys() if k >= bid]
        sold_keys_to_cancel = [k for k in sold.keys() if k <= ask]

        orders_to_cancel = itertools.chain(
            itertools.chain.from_iterable(
                bought.pop(k) for k in bought_keys_to_cancel),
            itertools.chain.from_iterable(
                sold.pop(k) for k in sold_keys_to_cancel))

        # Cancel buy orders before selling at the same price
        tasks = [
            loop.create_task(api.cancel(venue, stock, order.get('id')))
            for order in orders_to_cancel]
        asyncio.as_completed(tasks)

        if api.shares > 0 and ask > 100 and len(sold) < QUEUE_LENGTH:
            response = await api.sell(
                venue, stock, ask, s_ask, 'limit')
            price = response.get('price')
            if price:
                sold[price].append(response)
        response = await api.buy(
            venue, stock, bid, s_bid, 'limit')
        price = response.get('price')
        if price:
            bought[price].append(response)

async def bookloop(api, venue, stock):
    while True:
        await asyncio.sleep(1)
        await api.get_orderbook(venue, stock)

async def status_loop(api):
    while True:
        await asyncio.sleep(1)
        await api.get_status()

async def main(options):
    stock = 'FOOBAR'
    venue = 'TESTEX'
    account = 'EXB123456'
    api_key = '7eebfee0e3e3cf8e5559652890e0ac120f7cc6fc'
    api = Api(api_key=api_key)
    if options.start_new or options.resume:
        if options.start_new:
            result = await api.start_level()
        else:
            result = await api.resume_level()
            if not isinstance(result, dict) or not result.get('ok'):
                result = await api.start_level()
        stock = result.get('tickers', [])[0]
        venue = result.get('venues', [])[0]
        account = result.get('account')
        api.account = account
        orders = await api.get_orders_status(venue)
        if 'order' in orders:
            for order in orders.get('orders'):
                loop.create_task(api.cancel(venue, stock, order.get('id')))

    options = {
        'stock': stock,
        'venue': venue
    }
    return api, options


if __name__ == '__main__':
    limit_bid = 0
    limit_ask = 10000
    args = argparse.ArgumentParser()
    args.add_argument(
        '--start_new', action='store_true', default=False,
        help='Starts a new level')
    args.add_argument(
        '--resume', action='store_true', default=False,
        help='Resumes the level')
    parsed = args.parse_args()
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    main_func = asyncio.ensure_future(main(parsed))
    loop.run_until_complete(
        asyncio.wait([main_func], loop=loop))

    api, options = main_func.result()
    venue = options['venue']
    stock = options['stock']

    ticker_func = api.get_ticker(venue, stock)
    fills_func = api.get_fills(venue)
    buy_func = buy_loop(
        api, venue, stock, limit_bid, limit_ask)
    status_func = status_loop(api)
    tasks = [
        asyncio.ensure_future(ticker_func),
        asyncio.ensure_future(status_func),
        asyncio.ensure_future(buy_func),
        asyncio.ensure_future(fills_func),
    ]
    loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()
